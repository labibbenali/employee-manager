package com.employeemanager;

import com.employeemanager.model.Employee;
import com.employeemanager.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeResource  {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeResource(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
//-------------------------------------------------------------------------------------------
    //get all employee
    @GetMapping("/all")
        public ResponseEntity<List<Employee>> getAllEmployees(){
        List<Employee> employees = employeeService.findAllEmployees();
        return new ResponseEntity<>(employees, HttpStatus.OK);
        }

//-------------------------------------------------------------------------------------------
    // get employee by id
    @GetMapping("/find/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long id){
        Employee employee = employeeService.findEmployeeById(id);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }
//---------------------------------------------------------------------------------------------
    // add employee
    @PostMapping("/add")
    public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee){
        Employee newEmployee = employeeService.addEmployee(employee);
        return new ResponseEntity<>(newEmployee, HttpStatus.CREATED);
    }

//------------------------------------------------------------------------------------------------
    //update employee
@PutMapping("/update")
public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee){
            Employee updatedEmployee = employeeService.updateEmployee(employee);
            return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);

}
//--------------------------------------------------------------------------------------------------
    //delete employee
@DeleteMapping("/delete/{id}")
public ResponseEntity<?> deleteEmployee( @PathVariable("id") Long id) {
    employeeService.deleteEmploye(id);
    return new ResponseEntity<>(HttpStatus.OK);
    }


}
