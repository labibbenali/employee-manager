package com.employeemanager.service;
import com.employeemanager.exception.UserNotFoundException;
import com.employeemanager.model.Employee;
import com.employeemanager.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class EmployeeService {
    private final EmployeeRepo employeeRepo;

    @Autowired
    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }
    //------------------------------------------------------------------------------------
    //add employee
    public Employee addEmployee(Employee employee){
     employee.setEmployeeCode(UUID.randomUUID().toString());
     return employeeRepo.save(employee);
    }
    //------------------------------------------------------------------------------------
    //find all employee
    public List<Employee> findAllEmployees(){
        return employeeRepo.findAll();
    }
    //-------------------------------------------------------------------------------------
    //update employee
    public Employee updateEmployee(Employee employee){
        return employeeRepo.save(employee);
    }
    //--------------------------------------------------------------------------------------
    //find employee by Id
    public Employee findEmployeeById(Long id){
    return employeeRepo.findEmployeeById( id)
            .orElseThrow(() -> new UserNotFoundException("User by id"+id+"was not found"));
    }
    //------------------------------------------------------------------------------------------
    //delete employee
    public void deleteEmploye(Long id){
        employeeRepo.deleteEmployeeById(id);
    }

}
