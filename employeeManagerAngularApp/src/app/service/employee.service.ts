import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Employee } from '../model/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http : HttpClient) { }
// get methode 
  public getEmployees():Observable<Employee[]>{
    return this.http.get<Employee[]>(`${this.apiServerUrl}/employee/all`);
    
  }
//post methode
  public addEmployee(employee : Employee):Observable<Employee>{
    return this.http.post<Employee>(`${this.apiServerUrl}/employee/add`, employee);
    
  }
// put methode for update
public updateEmployee(employee : Employee):Observable<Employee>{
  return this.http.put<Employee>(`${this.apiServerUrl}/employee/update`, employee);
  
}
//delete methode
public deleteEmployee(employeeId : number):Observable<void>{
  return  this.http.delete<void>(`${this.apiServerUrl}/employee/delete/${employeeId}`);
  
}

}
