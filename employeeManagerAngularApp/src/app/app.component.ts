import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Employee } from './model/employee';
import { EmployeeService } from './service/employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public employees !: Employee[];
  public editEmployee!: any;
  public deleteEmployee!: any;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  public getEmployees() {
    this.employeeService.getEmployees()
      .subscribe({
        next: (response: Employee[]) => {
          this.employees = response;
          console.log(this.employees);
        },
        error: (error: HttpErrorResponse) => {
          alert(error.message);
        }
      });
  }

  //--------------------------------------------------------------------------
  public closeAdd(addForm: NgForm) {
    addForm.reset();
  }
  //---------------------------------------------------------------------------
  public onAddEmloyee(addForm: NgForm): void {
    this.employeeService.addEmployee(addForm.value)
      .subscribe({
        next: (response: Employee) => {
          console.log(response);
          this.getEmployees();
        },
        error: (error: HttpErrorResponse) => { alert(error.message); }
      });
    addForm.reset();
    document.getElementById("add-employee-form")?.click();
  }
  //--------------------------------------------------------------------------------
  public onUpdateEmployee(employee: Employee): void {
    this.employeeService.updateEmployee(employee)
      .subscribe({
        next: (response: Employee) => {
          console.log(response);
          this.getEmployees();
        },
        error: (error: HttpErrorResponse) => { alert(error.message); }
      });

  }
  //-----------------------------------------------------------------------------------
  public onDeleteEmployee(employeeId: number): void {
    this.employeeService.deleteEmployee(employeeId)
      .subscribe({
        next: (response: void) => {
          console.log(response);
          this.getEmployees();
        },
        error: (error: HttpErrorResponse) => { alert(error.message); }
      });

  }
  //--------------------------------------------------------------------------------
  public searchEmployee(key:string):void{
    console.log(key);
    const results:Employee[]=[];
    for(const employee of this.employees){
      if(employee.name.toLocaleLowerCase().indexOf(key.toLocaleLowerCase())!==-1
      || employee.email.toLocaleLowerCase().indexOf(key.toLocaleLowerCase())!==-1
      || employee.phone.toLocaleLowerCase().indexOf(key.toLocaleLowerCase())!==-1
      || employee.jobTitle.toLocaleLowerCase().indexOf(key.toLocaleLowerCase())!==-1 ){
          results.push(employee);
      }
    }
    this.employees = results;
    if(results.length===0 || !key){
      this.getEmployees();
    }
  }
  //--------------------------------------------------------------------------------
  public onOpenModal(mode: String, employee?: Employee): void {
    const container = document.getElementById("main-container");

    const button = document.createElement('button');
    button.type = "button";
    button.style.display = "none";
    button.setAttribute("data-bs-toggle", "modal");
    if (mode === "add") {
      button.setAttribute("data-bs-target", "#addEmployeeModal");
    }
    if (mode === "edit") {
      this.editEmployee = employee;
      button.setAttribute("data-bs-target", "#updateEmployeeModal");
    }
    if (mode === "delete") {
      this.deleteEmployee = employee;
      button.setAttribute("data-bs-target", "#deleteEmployeeModal");
    }
    container?.appendChild(button);
    button.click();
  }
  //------------------------------------------------------------------------------------------


}